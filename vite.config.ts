/*
 * @Author: kxl 939624959@qq.com
 * @Date: 2023-06-21 16:06:31
 * @LastEditors: wangyaofeng 1158983938@qq.com
 * @LastEditTime: 2024-08-06 14:13:28
 * @Description: 请输入文件描述
 */
// import buildInfoPlugin from './vitePlugins/buildInfoPlugin'
import vue from '@vitejs/plugin-vue'
// 如果编辑器提示 path 模块找不到，则可以安装一下 @types/node -> npm i @types/node -D
import { resolve } from 'path'
import { defineConfig } from 'vite'
import viteCompression from 'vite-plugin-compression'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    // buildInfoPlugin(),
    vue(),
    viteCompression({
      verbose: true,
      disable: false,
      threshold: 10240,
      algorithm: `gzip`,
      ext: `.gz`,
    }),
  ],
  // 配置别名
  resolve: {
    alias: {
      '@': resolve(__dirname, `src`),
      '@assets': resolve(__dirname, `src/assets`),
      '@router': resolve(__dirname, `src/router`),
      '@store': resolve(__dirname, `src/store`),
      '@components': resolve(__dirname, `src/components`),
      '@views': resolve(__dirname, `src/views`),
    },
  },
  // CSS 预处理器
  css: {
    preprocessorOptions: {
      less: {
        modifyVars: {
          hack: `true; @import (reference) "${resolve(`src/assets/less/index.less`)}";`, //将ant里面所有的变量引进来
        },
        javascriptEnabled: true,
      },
    },
  },
  base: `./`, // 设置打包路径
  //启动服务配置
  server: {
    host: `0.0.0.0`,
    port: 8010,
    open: true, // 设置服务启动时是否自动打开浏览器
    cors: true, // 允许跨域
    // 设置代理，根据我们项目实际情况配置
    // proxy: {
    //   '/api': {
    //     target: 'http://xxx.xxx.xxx.xxx:8000',
    //     changeOrigin: true,
    //     secure: false,
    //     rewrite: (path) => path.replace('/api/', '/')
    //   }
    // }
  },
  //去除 console debugger
  build: {
    sourcemap: true,
    terserOptions: {
      compress: {
        drop_console: true,
        drop_debugger: true,
      },
    },
    rollupOptions: {
      maxParallelFileOps: 1,
      output: {
        // 最小化拆分包
        manualChunks(id) {
          if (id.includes(`node_modules`)) {
            // 通过拆分包的方式将所有来自node_modules的模块打包到单独的chunk中
            return id.toString().split(`node_modules/`)[1].split(`/`)[0].toString()
          }
        },
        // 设置chunk的文件名格式
        chunkFileNames: chunkInfo => {
          const facadeModuleId = chunkInfo.facadeModuleId ? chunkInfo.facadeModuleId.split(`/`) : []
          const fileName1 = facadeModuleId[facadeModuleId.length - 2] || `[name]`
          // 根据chunk的facadeModuleId（入口模块的相对路径）生成chunk的文件名
          return `js/${fileName1}/[name].[hash].js`
        },
        // 设置入口文件的文件名格式
        entryFileNames: `js/[name].[hash].js`,
        // 设置静态资源文件的文件名格式
        assetFileNames: `[ext]/[name].[hash:4].[ext]`,
      },
    },
  },
})
