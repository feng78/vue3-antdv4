/*
 * @Author: wangyaofeng 1158983938@qq.com
 * @Date: 2023-05-12 09:20:50
 * @LastEditors: wangyaofeng 1158983938@qq.com
 * @LastEditTime: 2024-08-06 15:14:16
 * @Description: 全局组件添加
 */
/**  添加全局组件名字简单的最好加个q 避免与局部组件命名冲突 */
// import QFormItem from './formItem/FormItem.vue'
// import QInputSearch from './formItem/QInputSearch.vue'
// import QIcon from './icon/QIcon.vue'
// import LeftAndRightLayout from './pageTemplate/LeftAndRightLayout.vue'
// import QPagination from './qPagination/QPagination.vue'
// import QTable from './table/qTable/QTable.vue'
// import QTitle from './title/QTitle.vue'
// import QTree from './tree/qTree/QTree.vue'

import * as Icons from '@ant-design/icons-vue'

const icons: any = Icons
export default function install(app: any) {
  for (const i in Icons) {
    if (i !== `createFromIconfontCN`) {
      app.component(i, icons[i])
    }
  }
  // app.component(`QPagination`, QPagination)
  // app.component(`QTable`, QTable)
  // app.component(`QTitle`, QTitle)
  // app.component(`QIcon`, QIcon)
  // app.component(`LeftAndRightLayout`, LeftAndRightLayout)
  // app.component(`QFormItem`, QFormItem)
  // app.component(`QInputSearch`, QInputSearch)
  // app.component(`QTree`, QTree)
}
