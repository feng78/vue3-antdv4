/*
 * @Author: wangyaofeng 1158983938@qq.com
 * @Date: 2023-05-25 17:11:23
 * @LastEditors: wangyaofeng 1158983938@qq.com
 * @LastEditTime: 2024-08-06 17:14:11
 * @Description: 入口文件
 */
import { createRouter, createWebHashHistory } from 'vue-router'

import { useRouteStore } from '@/store/route'

import routes from './routeList'

const router = createRouter({
  history: createWebHashHistory(),
  routes,
})
// router.beforeEach((to, from, next) => {
// })

router.afterEach((to, from) => {
  useRouteStore().addCachedRoute(to)
})

export default router
