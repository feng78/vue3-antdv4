/*
 * @Author: wangyaofeng 1158983938@qq.com
 * @Date: 2023-05-17 16:55:05
 * @LastEditors: wangyaofeng 1158983938@qq.com
 * @LastEditTime: 2023-05-26 10:59:27
 * @Description: 全局路由类型
 */
import 'vue-router' 

declare module 'vue-router' {
  interface RouteMeta {
    title: string
    description?: string
    hidden?: boolean
    icon?: string
    htmlSrc?: string
    excludeMultiTab?: boolean
    childApp?:string
  }
}
