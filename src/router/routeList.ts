/*
 * @Author: wangyaofeng 1158983938@qq.com
 * @Date: 2023-05-12 09:20:50
 * @LastEditors: wangyaofeng 1158983938@qq.com
 * @LastEditTime: 2024-08-06 17:48:02
 * @Description: 本地路由列表
 */
import { RouteRecordRaw } from 'vue-router'
import { flattenTreeData } from '@/utils'

import Empty from '@/layout/EmptyPage.vue'

export const menuList = [
  {
    path: `/home`,
    name: `Home`,
    component: () => import(`@/views/home/HomePage.vue`),
    meta: {
      title: `首页`,
      icon: 'HomeOutlined',
    },
  },
  {
    path: `/menu1`,
    name: `menu1`,
    meta: {
      title: `菜单1`,
      icon: 'HomeOutlined',
    },
    children: [
      {
        path: `/page1`,
        name: `page1`,
        component: () => import(`@/views/home/HomePage.vue`),
        meta: {
          title: `页面1`,
        },
      },
    ]
  },
  {
    path: `/menu2`,
    name: `menu2`,
    meta: {
      title: `菜单2`,
      icon: 'HomeOutlined',
    },
    children: [
      {
        path: `/page2`,
        name: `page2`,
        component: () => import(`@/views/home/HomePage.vue`),
        meta: {
          title: `页面2`,
        },
      },
    ]
  },
]

let routes: RouteRecordRaw[] = [
  {
    path: `/`,
    name: `LayoutPage`,
    redirect: 'home',
    component: () => import(`@/layout/LayoutPage.vue`),
    children: [
      {
        path: `/empty`,
        name: `Empty`,
        component: Empty, //取消路由懒加载 修复多页签第一次刷新tabs闪动的bug
        meta: {
          title: `空页面`,
          hidden: true,
          excludeMultiTab: true,
        },
      },
      ...flattenTreeData(menuList).filter(item => item.component) as RouteRecordRaw[]
    ],
  },
]


export default routes