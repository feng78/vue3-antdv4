/*
 * @Author: kxl 939624959@qq.com
 * @Date: 2023-10-13 09:45:45
 * @LastEditors: wangyaofeng 1158983938@qq.com
 * @LastEditTime: 2024-08-06 15:12:44
 * @Description: 请输入文件描述
 */
import { createApp } from 'vue'
import Antd from 'ant-design-vue'

import App from './App.vue'


import '@/plugins'
//https://animate.style/
import '@assets/less/index.less'
import components from '@/components'
import router from '@router/index'
import { createPinia } from 'pinia'
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'

const app = createApp(App)
app.use(createPinia().use(piniaPluginPersistedstate)).use(router).use(Antd).use(components).mount(`#app`)

app.config.errorHandler = (err, instance, info) => {
    // eslint-disable-next-line
    console.log(err, instance, info)
}

app.config.warnHandler = (msg, instance, trace) => {
    // eslint-disable-next-line
    console.log(msg, instance, trace)
}

