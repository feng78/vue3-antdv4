/*
 * @Author: wangyaofeng 1158983938@qq.com
 * @Date: 2023-05-12 09:20:50
 * @LastEditors: wangyaofeng 1158983938@qq.com
 * @LastEditTime: 2024-08-06 17:12:11
 * @Description: 入口文件
 */
//base64 转 blob
export const dataURLtoBlob = (dataUrl: string) => {
  const arr: any[] = dataUrl.split(`,`)
  const mime = arr[0].match(/:(.*?);/)[1]
  const bstr = window.atob(arr[1])
  let n = bstr.length
  const u8arr = new Uint8Array(n)
  while (n--) {
    u8arr[n] = bstr.charCodeAt(n)
  }
  return URL.createObjectURL(new Blob([u8arr], { type: mime }))
}

//生成唯一id
export const generateUuid = (): string => {
  let d = new Date().getTime()
  if (window.performance && typeof window.performance.now === `function`) {
    d += performance.now()
  }
  const uuid = `xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx`.replace(/[xy]/g, function (c) {
    const r = (d + Math.random() * 16) % 16 | 0
    d = Math.floor(d / 16)
    return (c == `x` ? r : (r & 0x3) | 0x8).toString(16)
  })
  return uuid.replace(/-/g, ``)
}

/** 函数防抖 */
export function debounce<T, F extends (this: T, ...args: any[]) => any>(
  func: F,
  wait: number = 500,
  immediate: boolean = true
): (...args: Parameters<F>) => void {
  let timeout: NodeJS.Timeout | null
  return function (this: T, ...args: Parameters<F>) {
    const later = () => {
      timeout = null
      if (!immediate) func.apply(this, args)
    }
    const callNow = immediate && !timeout
    clearTimeout(timeout!)
    timeout = setTimeout(later, wait)
    if (callNow) func.apply(this, args)
  }
}

//文件下载
export const downloadFile = (url: string) => {
  const frame = document.createElement(`iframe`)
  frame.src = url
  frame.style.display = `none`
  document.body.appendChild(frame)
}

//分时函数
export const splitTime = <T, Args extends any[]>(
  dataArray: T[],
  processFunction: (item: T, index: number, ...args: Args) => void,
  ...args: Args
): Promise<void> => {
  return new Promise<void>(res => {
    if (typeof dataArray === `number`) {
      dataArray = new Array(dataArray).fill(0) as T[]
    }
    let index = 0

    const handleIdle = (deadline: IdleDeadline) => {
      while (index < dataArray.length && deadline.timeRemaining() > 0) {
        processFunction(dataArray[index], index, ...args)
        index++
      }

      if (index < dataArray.length) {
        requestIdleCallback(handleIdle)
      } else {
        res()
      }
    }
    requestIdleCallback(handleIdle)
  })
}

export * from './arrHandle'
export * from './calculation'
export * from './dom'
export * from './number'
export * from './objHandle'
export * from './treeDataHandle'
