/*
 * @Author: wangyaofeng 1158983938@qq.com
 * @Date: 2023-05-12 09:20:50
 * @LastEditors: wangyaofeng 1158983938@qq.com
 * @LastEditTime: 2024-02-02 10:45:19
 * @Description: 数组拓展操作方法
 */
/**
 * 数组方法拓展
 * batchDelete 批量删除复合条件的数据 返回删除数组  会改变原始数组。
 */

/**
 * 批量删除符合条件的数据。返回被删除的元素组成的数组。会改变原始数组。
 * @param arr - 需要操作的数组
 * @param callBack - 回调函数，返回 true 的元素将被删除
 * @returns 返回被删除的元素的数组，元素的顺序与在原数组中的顺序相同
 */
const batchDelete = <T>(arr: T[], callBack: (item: T, index: number) => boolean): T[] => {
  const deleteArr: T[] = [] // 存储被删除的元素
  for (let index = arr.length - 1; index >= 0; index--) {
    if (callBack(arr[index], index)) {
      // 如果回调函数返回 true，删除元素并将其添加到 deleteArr
      deleteArr.push(...arr.splice(index, 1))
    }
  }
  // 反转 deleteArr 以保持元素的原始顺序
  return deleteArr.reverse()
}

/**
 * 从数组中过滤出具有唯一字段值的对象。
 * 该函数接受一个对象数组和一个字段名作为参数，并返回一个新数组，
 * 该数组只包含具有唯一指定字段值的对象。
 * @param {{ [key: string]: any }[]} arr - 要过滤的对象数组。
 * @param {string} field - 用于确定对象唯一性的字段名。
 * @returns {{ [key: string]: any }[]} - 过滤后的数组，其中每个对象的指定字段值都是唯一的。
 */

const uniqueByField = <T extends Record<string, any>>(arr: T[], field: string): T[] => {
  // 创建一个 Set 来跟踪已经见过的字段值。
  const seen = new Set()

  // 使用 filter 方法过滤数组。
  // 对于数组中的每个对象，我们检查其指定字段的值是否已在 "seen" Set 中。
  // 如果是，则我们不将其包括在结果数组中。
  // 如果不是，我们将其添加到 "seen" Set 并将其包括在结果数组中。
  return arr.filter(item => {
    const key = item[field]
    if (seen.has(key)) {
      return false
    }
    seen.add(key)
    return true
  })
}

export { batchDelete, uniqueByField }
