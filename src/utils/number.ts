/*
 * @Author: DESKTOP-R2S5SLJ\Lenovo 861677874@qq.com
 * @Date: 2023-09-21 11:01:24
 * @LastEditors: kxl 939624959@qq.com
 * @LastEditTime: 2023-11-29 13:58:23
 * @Description: 数字类型处理
 */

/**根据数值的大小，将数字转换为以千（K）、百万（M）、十亿（B）、或万亿（T）为单位的字符串表示 */
const numberFormatter = (num: number): string => {
  if (Math.abs(num) >= 1_000_000_000_000) {
    return `${Math.sign(num) * parseFloat((Math.abs(num) / 1_000_000_000_000).toFixed(2))}T`
  } else if (Math.abs(num) >= 1_000_000_000) {
    return `${Math.sign(num) * parseFloat((Math.abs(num) / 1_000_000_000).toFixed(2))}B`
  } else if (Math.abs(num) >= 1_000_000) {
    return `${Math.sign(num) * parseFloat((Math.abs(num) / 1_000_000).toFixed(2))}M`
  } else if (Math.abs(num) > 999) {
    return `${Math.sign(num) * parseFloat((Math.abs(num) / 1_000).toFixed(2))}K`
  } else {
    return (Math.sign(num) * Math.abs(num)).toString()
  }
}

/**将数字转换为百分比显示 */
const percentageString = (num: number): string => `${(num * 100).toFixed(2)}%`

/**根据文件大小将字节数（Bytes）转换成更合适的单位（KB、MB、GB） */
const bytesFormatter = (bytes: number): string => {
  if (bytes < 1024) {
    return `${bytes} B`
  } else if (bytes < 1024 * 1024) {
    return `${(bytes / 1024).toFixed(2)} KB`
  } else if (bytes < 1024 * 1024 * 1024) {
    return `${(bytes / 1024 / 1024).toFixed(2)} MB`
  } else {
    return `${(bytes / 1024 / 1024 / 1024).toFixed(2)} GB`
  }
}

export { bytesFormatter, numberFormatter, percentageString }
