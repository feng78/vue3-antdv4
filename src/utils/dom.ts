/*
 * @Author: wangyaofeng 1158983938@qq.com
 * @Date: 2023-05-12 09:20:50
 * @LastEditors: wangyaofeng 1158983938@qq.com
 * @LastEditTime: 2023-05-26 11:05:50
 * @Description: 涉及dom操作相关
 */
/** 获取文本宽度 */
const getTextWidth = (text: string, font = `normal 14px sans-serif`) => {
  const canvas = document.createElement(`canvas`)
  const context: any = canvas.getContext(`2d`)
  context.font = font
  const metrics = context.measureText(text)
  return metrics.width
}

/** 获取滚动条宽度 */
const getScrollbarWidth = () => {
  const outer: any = document.createElement(`div`)
  outer.style.visibility = `hidden`
  outer.style.width = `100px`
  outer.style.position = `absolute`
  outer.style.top = `-9999px`
  document.body.appendChild(outer)
  const widthNoScroll = outer.offsetWidth // 获取没有滚动条的元素宽度
  outer.style.overflow = `scroll` // 强制滚动条
  const inner = document.createElement(`div`) // 内部创建元素
  inner.style.width = `100%`
  outer.appendChild(inner)
  const widthWithScroll = inner.offsetWidth // 获取内部元素的可视区域，有滚动条
  outer.parentNode.removeChild(outer) // 移除创建的节点
  return widthNoScroll - widthWithScroll // 获取浏览器滚动条宽度
}

/** 文件下载 */
const downloadFile = (url: string) => {
  const frame = document.createElement(`iframe`)
  frame.src = url
  frame.style.display = `none`
  document.body.appendChild(frame)
}

/** Blob文件下载 */
const downloadFileByBlob = (data: any, fileName: string = `download`) => {
  const url = window.URL.createObjectURL(new Blob([data]))
  const link = document.createElement(`a`)
  link.href = url
  link.setAttribute(`download`, `${fileName}.json`)
  document.body.appendChild(link)
  link.click()
}

export { downloadFile, downloadFileByBlob, getScrollbarWidth, getTextWidth }
