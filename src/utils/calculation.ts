/*
 * @Author: wangyaofeng 1158983938@qq.com
 * @Date: 2023-05-12 09:20:50
 * @LastEditors: wangyaofeng 1158983938@qq.com
 * @LastEditTime: 2023-05-26 11:05:40
 * @Description: 精度计算
 */
/**
 * 检查输入是否为数字
 * @param input - 任何输入
 * @returns 如果输入是数字，则返回 true，否则返回 false
 */
export function checkNumber(input: any): input is number {
  return typeof input === `number` && !isNaN(input)
}

/**
 * 精确除法函数
 * @param arg1 - 被除数
 * @param arg2 - 除数
 * @returns 返回 arg1 除以 arg2 的精确结果
 * @throws 如果参数不是数字，将抛出错误
 */
export function accDiv(arg1: number, arg2: number) {
  if (!checkNumber(arg1) || !checkNumber(arg2)) {
    throw new Error(`无效的参数：两个参数都必须是数字。`)
  }

  let t1 = 0,
    t2 = 0
  try {
    t1 = arg1.toString().split(`.`)[1].length
  } catch (e) {
    console.error(e)
  }
  try {
    t2 = arg2.toString().split(`.`)[1].length
  } catch (e) {
    console.error(e)
  }

  const r1 = Number(arg1.toString().replace(`.`, ``))
  const r2 = Number(arg2.toString().replace(`.`, ``))

  return (r1 / r2) * Math.pow(10, t2 - t1)
}

/**
 * 精确乘法函数
 * @param arg1 - 乘数
 * @param arg2 - 被乘数
 * @returns 返回 arg1 乘以 arg2 的精确结果
 * @throws 如果参数不是数字，将抛出错误
 */
export function accMul(arg1: number, arg2: number) {
  if (!checkNumber(arg1) || !checkNumber(arg2)) {
    throw new Error(`无效的参数：两个参数都必须是数字。`)
  }

  let m = 0
  const s1 = arg1.toString()
  const s2 = arg2.toString()
  try {
    m += s1.split(`.`)[1].length
  } catch (e) {
    console.error(e)
  }
  try {
    m += s2.split(`.`)[1].length
  } catch (e) {
    console.error(e)
  }

  return (Number(s1.replace(`.`, ``)) * Number(s2.replace(`.`, ``))) / Math.pow(10, m)
}

/**
 * 精确加法函数
 * @param arg1 - 加数
 * @param arg2 - 被加数
 * @returns 返回 arg1 加上 arg2 的精确结果
 * @throws 如果参数不是数字，将抛出错误
 */
export function accAdd(arg1: number, arg2: number) {
  if (!checkNumber(arg1) || !checkNumber(arg2)) {
    throw new Error(`无效的参数：两个参数都必须是数字。`)
  }

  let r1 = 0,
    r2 = 0
  try {
    r1 = arg1.toString().split(`.`)[1].length
  } catch (e) {
    console.error(e)
  }
  try {
    r2 = arg2.toString().split(`.`)[1].length
  } catch (e) {
    console.error(e)
  }

  const m = Math.pow(10, Math.max(r1, r2))
  return (arg1 * m + arg2 * m) / m
}

/**
 * 精确减法函数
 * @param arg1 - 被减数
 * @param arg2 - 减数
 * @returns 返回 arg1 减去 arg2 的精确结果
 * @throws 如果参数不是数字，将抛出错误
 */
export function accSubtr(arg1: number, arg2: number) {
  if (!checkNumber(arg1) || !checkNumber(arg2)) {
    throw new Error(`无效的参数：两个参数都必须是数字。`)
  }

  let r1 = 0,
    r2 = 0
  try {
    r1 = arg1.toString().split(`.`)[1].length
  } catch (e) {
    console.error(e)
  }
  try {
    r2 = arg2.toString().split(`.`)[1].length
  } catch (e) {
    console.error(e)
  }

  const m = Math.pow(10, Math.max(r1, r2))
  const n = r1 >= r2 ? r1 : r2
  return Number(((arg1 * m - arg2 * m) / m).toFixed(n))
}
