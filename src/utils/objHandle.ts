/*
 * @Author: wangyaofeng 1158983938@qq.com
 * @Date: 2023-05-23 17:36:16
 * @LastEditors: wangyaofeng 1158983938@qq.com
 * @LastEditTime: 2023-08-17 15:43:05
 * @Description: 对象拓展操作方法
 */
/**
 * 对象方法拓展
 * deepEqual 比较两个对象是否相同
 * deepCopy  对象深拷贝（正常用Json就行，除非对象中有函数这些json照顾不到的用这个函数）
 */

/**
 * 深度比较两个对象是否具有相同的值 忽略属性的顺序
 * @param obj1 - 第一个对象
 * @param obj2 - 第二个对象
 * @returns 如果两个对象具有相同的值，则返回 true，否则返回 false
 */
function deepEqual(obj1: any, obj2: any): boolean {
  // 获取对象的类型
  const type1 = Object.prototype.toString.call(obj1)
  const type2 = Object.prototype.toString.call(obj2)

  // 类型不同，直接返回 false
  if (type1 !== type2) {
    return false
  }

  // 对于数组和对象，检查每个元素/属性
  if (type1 === `[object Array]` || type1 === `[object Object]`) {
    const keys1 = Object.keys(obj1)
    const keys2 = Object.keys(obj2)

    // 键的数量不同，直接返回 false
    if (keys1.length !== keys2.length) {
      return false
    }

    // 深度比较每个元素/属性
    for (const key of keys1) {
      if (!keys2.includes(key) || !deepEqual(obj1[key], obj2[key])) {
        return false
      }
    }
  }
  // 对于日期，比较时间戳
  else if (type1 === `[object Date]`) {
    if (obj1.getTime() !== obj2.getTime()) {
      return false
    }
  }
  // 对于正则表达式，比较源字符串和标志
  else if (type1 === `[object RegExp]`) {
    if (obj1.source !== obj2.source || obj1.flags !== obj2.flags) {
      return false
    }
  }
  // 对于 Map，比较每个键值对
  else if (type1 === `[object Map]`) {
    if (obj1.size !== obj2.size) {
      return false
    }
    for (const [key, value] of obj1) {
      if (!obj2.has(key) || !deepEqual(value, obj2.get(key))) {
        return false
      }
    }
  }
  // 对于 Set，检查每个元素
  else if (type1 === `[object Set]`) {
    if (obj1.size !== obj2.size) {
      return false
    }
    for (const item of obj1) {
      if (!obj2.has(item)) {
        return false
      }
    }
  }
  // 对于其它类型，进行严格比较
  else if (obj1 !== obj2) {
    return false
  }

  // 所有检查都通过，返回 true
  return true
}

/**
 * 深拷贝函数，用于复制复杂对象，考虑所有的情况，包括数组、函数、日期、正则表达式、Map、Set、以及循环引用等情况
 * @param {unknown} obj - 需要拷贝的对象
 * @param {WeakMap<object, any>} hash - 用于处理循环引用的WeakMap
 * @returns {unknown} - 返回复制后的对象
 */
function deepCopy<T>(obj: T, hash = new WeakMap<object, T>()): T {
  if (obj == null || typeof obj !== `object`) return obj as T
  if (hash.has(obj as object)) return hash.get(obj as object)!
  let result: any

  if (obj instanceof Date) {
    result = new Date(obj)
  } else if (obj instanceof RegExp) {
    result = new RegExp(obj)
  } else {
    const isArr = Array.isArray(obj)
    result = isArr ? [] : {}
    hash.set(obj as object, result)

    if (isArr) {
      const arrObj = obj as unknown as T[]
      for (let i = 0; i < arrObj.length; i++) {
        result[i] = deepCopy(arrObj[i], hash)
      }
    } else {
      const recordObj = obj as unknown as Record<string, unknown>
      for (const key in recordObj) {
        if (Object.prototype.hasOwnProperty.call(recordObj, key)) {
          result[key] = deepCopy(recordObj[key], hash)
        }
      }
    }
  }

  return result as T
}

/**
 * 对象数组根据特定的key进行去重
 * @param arr 需要去重的数组
 * @param key 根据哪个key去重 默认是id
 * @returns  去重后的数组
 */
const removeDuplicate = (arr: any[], key = `id`) => {
  const res = new Map()
  return arr.filter(item => !res.has(item[key]) && res.set(item[key], 1))
}

/**
 * 对象数组根据特定的key进行分组
 * @param array
 * @param property
 * @returns 分组后的对象
 */
const groupBy = (array: any[], property: string) => {
  return array.reduce((result, current) => {
    ;(result[current[property]] = result[current[property]] || []).push(current)
    return result
  }, {})
}

export { deepCopy, deepEqual, groupBy, removeDuplicate }
