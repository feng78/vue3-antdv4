/*
 * @Author: wangyaofeng 1158983938@qq.com
 * @Date: 2024-07-31 17:13:41
 * @LastEditors: wangyaofeng 1158983938@qq.com
 * @LastEditTime: 2024-07-31 17:17:20
 * @Description: 全局类型声明
 */
declare global {
    type OptionInfo = {
      label:string
      value:string
      [key:string]:any
    }
}
  
export {};