/*
 * @Author: wangyaofeng 1158983938@qq.com
 * @Date: 2023-06-14 09:19:44
 * @LastEditors: kxl 939624959@qq.com
 * @LastEditTime: 2023-11-09 17:33:53
 * @Description: 通用ts方法
 */
/**数据映射 将对象中所有属性(深层) 都改为非必填 */
type BaseType = Number | String | Boolean | Symbol | null | undefined
export type DataMap<T> = {
  [P in keyof T]?: T[P] extends BaseType ? T[P] | undefined : DataMap<T[P]>
}

//将接口的部分属性改成必选, 其余保持不变
export type CustomRequired<T, K extends keyof T> = {
  [P in K]-?: T[P]
} & Omit<T, K>

//对象包含任意属性
export type CustomObj = {
  [x: string | number]: any
}

/** 解析Promise内类型 */
export type UnwrapPromise<T> = T extends Promise<infer U> ? U : T

export type PromiseReturnType<T extends (...args: any) => any> = UnwrapPromise<ReturnType<T>>
