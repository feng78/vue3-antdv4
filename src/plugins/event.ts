/*
 * @Author: wangyaofeng 1158983938@qq.com
 * @Date: 2023-05-12 09:20:50
 * @LastEditors: wangyaofeng 1158983938@qq.com
 * @LastEditTime: 2023-05-26 10:58:02
 * @Description: eventBus注册
 */
// import mitt from 'mitt'
// export default mitt()
/**
 * 事件总线 是一个非必要禁止使用的东西。很容易造成发布订阅的混乱
 * 本项目 仅供页面组件向页面外通讯的情况使用。使用的事件名称 和 用途一定要在这里加注释
 */
