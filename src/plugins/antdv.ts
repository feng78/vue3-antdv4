/*
 * @Author: wangyaofeng 1158983938@qq.com
 * @Date: 2023-05-12 09:20:50
 * @LastEditors: kxl 939624959@qq.com
 * @LastEditTime: 2023-06-26 11:12:44
 * @Description: ant组件全局设置
 */
//这里的改动 都是看了ant源码以后 打的补丁 随着版本升级 可能会失效 切记
import { Card, Cascader, Pagination, Select } from 'ant-design-vue'

Card.props.size.default = `small`
Card.props.bordered.default = false

Select.props.getPopupContainer.default = (trigger: Element) => trigger.parentNode
Select.props.showSearch.default = true
Select.props.allowClear.default = false
Select.props.filterOption.default = () => (input: string, option: any) => {
  return option.label.toLowerCase().indexOf(input.toLowerCase()) >= 0
}
Pagination.props.size = {
  type: String,
  default: `small`,
}
Pagination.props.showSizeChanger.default = true
Pagination.props.showQuickJumper.default = true
Pagination.props.showTotal = {
  type: Function,
  default: (total: any, range: any) => {
    return `共 ${total} 条`
  },
}

Cascader.props.showSearch.default = true
