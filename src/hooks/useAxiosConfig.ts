/*
 * @Author: wangyaofeng 1158983938@qq.com
 * @Date: 2023-05-12 09:20:50
 * @LastEditors: wangyaofeng 1158983938@qq.com
 * @LastEditTime: 2024-08-06 14:37:21
 * @Description: axios配置项[初次开发本项目请仔细阅读本连接](/src/types/services/http.ts)
 */
import { createApp } from 'vue'
import { notification } from 'ant-design-vue'

import PartLoading from '@/components/loading/PartLoading.vue'

import router from '@/router'

import { useMainStore } from '@/store'

import type { RequestConfig } from '@/types/services/http'
import type { AxiosError, AxiosInstance } from 'axios'
import axios from 'axios'

export default function (_axios: AxiosInstance) {
  /** 使用WeakMap类型的数据 键名所指向的对象可以被垃圾回收 避免dom对象的键名内存泄漏 */
  const loadingDom = new WeakMap()
  /** 添加loading */
  const setLoading = (config: RequestConfig) => {
    //添加按钮的loading
    if (config.loading) {
      config.loading.value = true
    }
    const loadingTarget = config.dom
    if (loadingTarget === undefined) return
    const loadingDomInfo = loadingDom.get(loadingTarget)
    if (loadingDomInfo) {
      loadingDomInfo.count++
    } else {
      const appExample = createApp(PartLoading)
      const loadingExample = appExample.mount(document.createElement(`div`)) as InstanceType<typeof PartLoading>
      loadingTarget.appendChild(loadingExample.$el)
      loadingExample.show(loadingTarget)
      loadingDom.set(loadingTarget, {
        count: 1, //记录当前dom的loading次数
        appExample,
      })
    }
  }
  /** 删除loading */
  const deleteLoading = (config: RequestConfig) => {
    //关闭组件的loading状态
    if (config.loading) {
      config.loading.value = false
    }
    const loadingTarget = config.dom
    if (loadingTarget === undefined) return
    const loadingDomInfo = loadingDom.get(loadingTarget)
    if (loadingDomInfo) {
      if (--loadingDomInfo.count === 0) {
        loadingDom.delete(loadingTarget)
        loadingDomInfo.appExample.unmount()
      }
    }
  }

  //取消重复请求
  const requestList = new Map()
  const cancelDuplicateRequest = (config: RequestConfig) => {
    if (config.isCancelToken) {
      const CancelToken = axios.CancelToken
      const source = CancelToken.source()
      config.cancelToken = source.token
      const requestUrl = config.url as string
      if (requestList.has(requestUrl)) {
        requestList.get(requestUrl).source.cancel({ config: requestList.get(requestUrl).config })
      } else {
        requestList.set(requestUrl, {})
      }
      requestList.get(requestUrl).source = source
      requestList.get(requestUrl).config = config
    }
  }

  //缓存重复请求
  const requestCached = new Map()
  type SetRequest = (callBack: (config: RequestConfig) => Promise<any>, config: RequestConfig) => Promise<any>
  const setRequestCached: SetRequest = async (callBack, config) => {
    //通过url和接口传参判断是否为同一个接口
    const key = config.url + JSON.stringify(config.params) + JSON.stringify(config.data)
    if (config?.isCached) {
      if (requestCached.has(key)) {
        return requestCached.get(key)
      } else {
        const data = await callBack(config)
        requestCached.set(key, data)
        return data
      }
    } else {
      return callBack(config)
    }
  }

  //缓存正在进行的请求
  const ongoingRequests = new Map()
  const uniqueRequestPerKey: SetRequest = async (callBack, config) => {
    //通过url和接口传参判断是否为同一个接口
    const key = config.url + JSON.stringify(config.params) + JSON.stringify(config.data)
    if (config.isUniqueRequest && ongoingRequests.has(key)) {
      return ongoingRequests.get(key)
    } else {
      const promise = callBack(config)
      ongoingRequests.set(key, promise)
      promise.finally(() => {
        ongoingRequests.delete(key)
      })
      return promise
    }
  }

  /** 配置axios的公共函数 */
  const setRequest: SetRequest = async (callBack, config) => {
    let httplist = JSON.parse(localStorage.getItem(`httpList`) as string)
    if (httplist) {
      if (!httplist.find((item: any) => item.url === config.url && item.baseURL === config.baseURL)) {
        httplist.push({
          url: config.url,
          params: config.params,
          data: config.data,
          baseURL: config.baseURL,
        })
      }
    } else {
      httplist = []
    }
    localStorage.setItem(`httpList`, JSON.stringify(httplist))

    setLoading(config)
    config = setPublicConfig(config)
    try {
      const requestFunc = (_config: RequestConfig) => setRequestCached(callBack, _config)
      const response = await uniqueRequestPerKey(requestFunc, config)
      deleteLoading(config)
      return response
    } catch (error) {
      deleteLoading(config)
      return Promise.reject(error)
    }
  }

  /** 设置公共配置处理 */
  const setPublicConfig = (config: RequestConfig): RequestConfig => {
    /**
     * 配置公共参数
     * 这个地方需要注意一下。因为拓展运算符的原因
     * config.params的值只能是普通对象。不考虑数组或FormData等结构。(治理工具配置保存接口需要参数为数组，所以增加了数组判断)
     * 数组的话让后台改传参结构
     * FormData的话请使用config.formData = true的配置
     */
    let params = Array.isArray(config.params) ? config.params : { ...config.params }
    if (config.formData) {
      const formData = new FormData()
      for (const key in params) {
        if (Array.isArray(params[key])) {
          params[key].forEach((item: any) => {
            formData.append(key, item)
          })
        } else {
          formData.append(key, params[key])
        }
      }
      params = formData
    }
    //配置不同请求方式的不同传参
    if ([`put`, `post`, `delete`, `path`].includes(config.method as string)) {
      config.data = params
      config.params = {}
    } else {
      config.params = params
    }
    //配置公共请求头
    config.headers = {}
    return config
  }

  /**
   * 网络请求错误处理函数
   * @param error 错误对象
   */
  const errorHandler = (error: AxiosError) => {
    if (String(error.code) === `400401`) {
      notification.warning({ message: `长时间未操作，请重新登陆！` })
      // eslint-disable-next-line
      console.log('长时间未操作，请重新登陆！')
      if (import.meta.env.MODE === `single`) {
        router.push({ name: `MultiCenterLogin` })
      } else {
        location.href = `${location.origin}/sso/login`
      }
    } else if (String(error.code) === `200401`) {
      notification.warning({ message: `登录异常` })
      // eslint-disable-next-line
      console.log(`登录异常`)

      if (import.meta.env.MODE === `single`) {
        router.push({ name: `MultiCenterLogin` })
      } else {
        location.href = `${location.origin}/sso/login`
      }
    } else if (String(error.code) === `500499`) {
      notification.warning({
        message: `参数异常`,
        description: error.message,
      })
    } else if (String(error.code) === `999999`) {
      notification.warning({
        message: error.message ? undefined : `系统异常`,
        description: error.message,
      })
    } else {
      notification.warning({
        message: error.code,
        description: error.message,
      })
    }
    return Promise.reject(error)
  }

  return {
    setRequest,
    setLoading,
    deleteLoading,
    cancelDuplicateRequest,
    errorHandler,
  }
}
