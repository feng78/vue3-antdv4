/*
 * @Author: wangyaofeng 1158983938@qq.com
 * @Date: 2023-05-12 09:20:50
 * @LastEditors: wangyaofeng 1158983938@qq.com
 * @LastEditTime: 2024-08-06 17:23:36
 * @Description: 路由菜单相关数据
 */
import router from '@/router'

import { defineStore } from 'pinia'
import type { RouteLocationNormalized } from 'vue-router'


type RouteStore = {
  activeRoute?: RouteLocationNormalized
  cachedRoute: RouteLocationNormalized[]
}

export const useRouteStore = defineStore({
  id: `route`,
  state: (): RouteStore => ({
    activeRoute: undefined,
    cachedRoute: [],
  }),
  actions: {


    /** 新增页签 */
    addCachedRoute(route: RouteLocationNormalized) {
      if (route.name === `Empty`) return
      if (route.meta.excludeMultiTab || !route.name || route.name === `LayoutPage`) {
        return true
      }
      const cachedRouteInfo = this.cachedRoute.find(
        item => decodeURIComponent(item.fullPath) === decodeURIComponent(route.fullPath)
      )
      if (!cachedRouteInfo) {
        this.cachedRoute.push(route)
      } else {
        // cachedRouteInfo.query = { ...route.query }
        // cachedRouteInfo.params = route.params
      }
      this.activeRoute = route
    },

    /** 删除页签 */
    async removeCachedRoute(removeFullPathList: string[]) {
      // 解码 removeFullPathList 中的每个 fullPath
      const decodedRemoveFullPathList = removeFullPathList.map(fullPath => decodeURIComponent(fullPath))
      this.cachedRoute = this.cachedRoute.filter(
        item => !decodedRemoveFullPathList.includes(decodeURIComponent(item.fullPath))
      )
      // 判断是否关闭当前标签，若关闭则跳转到最后一个还存在的标签页
      if (decodedRemoveFullPathList.includes(decodeURIComponent(this.activeRoute?.fullPath as string))) {
        this.activeRoute = this.cachedRoute[this.cachedRoute.length - 1]
        if (this.activeRoute) {
          await router.push(this.activeRoute)
        }
      }
    },

    /** 清空页签 */
    clearCachedRoute() {
      this.cachedRoute = []
    },

    /** 刷新页签 */
    async refRoute(fullPath: string) {
      if (fullPath.indexOf(`/`) !== 0) {
        console.warn(`refRoute方法传入的fullPath参数必须以/开头，否则会导致刷新失败`)
      }
      const route = this.cachedRoute.find(item => decodeURIComponent(item.fullPath) === decodeURIComponent(fullPath))
      if (!route) {
        router.push(fullPath)
        return
      }
      const _cachedRoute = this.cachedRoute
      this.cachedRoute = this.cachedRoute.filter(
        item => decodeURIComponent(item.fullPath) !== decodeURIComponent(fullPath)
      )
      await router.replace({ name: `Empty` })
      await router.replace(route)
      this.cachedRoute = _cachedRoute
    },

    /** 修改多页签路由名称 */
    openRouteRename(fullPath: string, title: string) {
      //延时，解决mrpv2列表跳转修改标题，只第一次跳转生效的bug
      setTimeout(() => {
        const cachedRouteInfo = this.cachedRoute.find(
          item => decodeURIComponent(item.fullPath) === decodeURIComponent(fullPath)
        )
        cachedRouteInfo && (cachedRouteInfo.meta.title = title)
      }, 100)
    },
  },
})
