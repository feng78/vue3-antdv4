/*
 * @Author: wangyaofeng 1158983938@qq.com
 * @Date: 2023-05-23 15:30:38
 * @LastEditors: wangyaofeng 1158983938@qq.com
 * @LastEditTime: 2024-08-06 14:44:16
 * @Description: 系统主要全局数据（用户 团队 系统配置等）
 */
import { generate } from '@ant-design/colors'
import { defineStore } from 'pinia'

interface ColorTheme {
  colorPrimary: string
  [key: string]: string
}
export type UseMainStore = {
  collapsed: boolean
  multiTab: boolean
  colorTheme: ColorTheme
}
export const useMainStore = defineStore({
  id: `main`,
  state: (): UseMainStore => ({
    collapsed: false,
    multiTab: true,
    colorTheme: {
      colorPrimary: `#2e5dfc`,
    },
  }),
  persist: {
    key: `mrpv3_mainStore`,
  },
  actions: {
    setColorTheme({ colorPrimary }: ColorTheme) {
      this.colorTheme.colorPrimary = colorPrimary
      document.documentElement.style.setProperty(`--ant-primary-color`, colorPrimary)
      // 生成一个颜色系列
      const colorPalette = generate(colorPrimary)
      colorPalette.forEach((item, index) => {
        document.documentElement.style.setProperty(`--ant-primary-${index + 1}`, item)
        this.colorTheme[`primary${index + 1}`] = item
      })
    },
  },
})
